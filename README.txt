A java library for doing simple physics simulations. Great for Android 2D games using the accelerometer data stream.

Examples can be found here:

https://bitbucket.org/hchughesix/labrinth2d-demos

contact: hughes036@gmail.com