//Copyright (c) 2014 H. C. Christopher Hughes
package com.hchughesix.mobile.labrinth2d;

import android.graphics.Point;
import android.graphics.RectF;

public abstract class PhysBarrier implements Phys{

    protected final RectF rect;

    protected PhysBarrier(RectF rect) {
        this.rect = rect;
    }

    public boolean contains(final Point point) {
        return rect.contains(point.x, point.y);
    }

}
