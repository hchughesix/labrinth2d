//Copyright (c) 2014 H. C. Christopher Hughes
package com.hchughesix.mobile.labrinth2d;

import android.graphics.Canvas;
import android.graphics.Point;

public interface Phys {

    public boolean isReady();

    void draw(Canvas canvas);

    Point getPoint();

    int getWidth();

    int getHeight();

}
