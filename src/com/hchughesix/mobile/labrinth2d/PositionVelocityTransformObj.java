//Copyright (c) 2014 H. C. Christopher Hughes
package com.hchughesix.mobile.labrinth2d;

import android.graphics.Point;

import java.util.Collection;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public class PositionVelocityTransformObj extends PositionVelocityTransform {
    protected final Collection<PhysBarrier> barriers;
    protected final Collection<Hole> holes;

    public PositionVelocityTransformObj(final PhysFreeMoving physFreeMoving, final Collection<PhysBarrier> barriers, final Collection<Hole> holes) {
        super(checkNotNull(physFreeMoving));
        checkNotNull(barriers);
        checkNotNull(holes);
        checkArgument(!barriers.contains(null));
        checkArgument(!holes.contains(null));
        this.barriers = barriers;
        this.holes = holes;
    }

    @Override
    public PositionVelocityTransform apply(final float x, final float y) {
        velNew = applyAccel(new float[]{x, y}, applyFriction(freeMoving.getVelocity()));
        final int yCoordProp = freeMoving.getPoint().y + (int)velNew[1];
        point.set(freeMoving.getPoint().x + (int)velNew[0], yCoordProp);
        final Point paddedPoint = addCollisionTestPadding(point.x, point.y, velNew);
        for(Hole hole : holes){
            if(hole.contains(new Point(paddedPoint.x, freeMoving.getPoint().y)) || hole.contains(new Point(freeMoving.getPoint().x, paddedPoint.y))){
                point.set(hole.getPoint().x+hole.getWidth()/2, hole.getPoint().y+hole.getHeight()/2);
                velNew = new float[]{0, 0};
                hole.addFreeMoving(freeMoving);
                return this;
            }
        }
        for(PhysBarrier barrier : barriers){
            //xCoord will be against the object
            if(barrier.contains(new Point(paddedPoint.x, freeMoving.getPoint().y))){
                point.x = freeMoving.getPoint().x;
                velNew[0] = -1*applyImpact(velNew[0]);
                break;
            }
        }
        for(PhysBarrier barrier : barriers){
            //yCoord will be against the object
            if(barrier.contains(new Point(freeMoving.getPoint().x, paddedPoint.y))){
                point.y = freeMoving.getPoint().y;
                velNew[1] = -1*applyImpact(velNew[1]);
                break;
            }
        }
        return this;
    }

}
