//Copyright (c) 2014 H. C. Christopher Hughes
package com.hchughesix.mobile.labrinth2d;

import android.graphics.*;
import com.google.common.collect.Lists;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

public class Hole extends PhysBarrier{

    public enum HoleDisplay {COUNT, POINTS, NONE}

    private final HoleDisplay type;
    private final int points;
    private final Paint holePaint = new Paint();
    private final Paint textPaint = new Paint();
    private final List<PhysFreeMoving> freeMovings = Lists.newArrayList();
    private int pointsTotal = 0;

    public Hole(final int color, final HoleDisplay type, final int points, final RectF rect) {
        super(checkNotNull(rect));
        this.holePaint.setColor(color);
        this.type = checkNotNull(type);
        this.points = points;
        textPaint.setColor(Color.RED);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setTextSize(35);
    }

    public int getPointsTotal() {
        return pointsTotal;
    }

    public void addFreeMoving(final PhysFreeMoving freeMoving){
        freeMovings.add(checkNotNull(freeMoving));
        pointsTotal = pointsTotal + (freeMoving.getScore() * points);
    }

    public Collection<PhysFreeMoving> getFreeMovings() {
        return Collections.unmodifiableCollection(freeMovings);
    }

    public void clearFreeMovings(){
        freeMovings.clear();
    }

    @Override
    public boolean isReady() {
        return true;
    }

    @Override
    public void draw(final Canvas canvas) {
        canvas.drawRect(rect, holePaint);
        switch (type){
            case COUNT:
                canvas.drawText(""+freeMovings.size(), rect.centerX(), rect.top, textPaint);
                break;
            case POINTS:
                canvas.drawText(""+pointsTotal, rect.centerX(), rect.top, textPaint);
                break;
            case NONE:
                break;
        }
    }

    @Override
    public Point getPoint() {
        return new Point((int)rect.left, (int)rect.top);
    }

    @Override
    public int getWidth() {
        return Math.round(rect.width());
    }

    @Override
    public int getHeight() {
        return Math.round(rect.height());
    }

}
