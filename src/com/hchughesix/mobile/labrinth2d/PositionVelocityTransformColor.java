//Copyright (c) 2014 H. C. Christopher Hughes
package com.hchughesix.mobile.labrinth2d;

import android.graphics.Bitmap;
import android.graphics.Point;

import static com.google.common.base.Preconditions.checkNotNull;

public class PositionVelocityTransformColor extends PositionVelocityTransform {

    private final Bitmap bitmap;
    private boolean inHole = false;
    private boolean finished = false;
    private final int barrierColor;
    private final int holeColor;
    private final int finishColor;

    public PositionVelocityTransformColor(final PhysFreeMoving freeMoving, final Bitmap bitmap, final int barrierColor, final int holeColor, final int finishColor) {
        super(checkNotNull(freeMoving));
        this.bitmap = checkNotNull(bitmap);
        this.barrierColor = barrierColor;
        this.holeColor = holeColor;
        this.finishColor = finishColor;
    }

    boolean isInHole(final int x, final int y){
        if(x < 0 || y < 0 || x >= bitmap.getWidth() || y >= bitmap.getHeight()){
            return false;
        }
        return holeColor == bitmap.getPixel(x, y);
    }

    boolean isAgainstWall(final int x, final int y){
        if(x < 0 || y < 0 || x >= bitmap.getWidth() || y >= bitmap.getHeight()){
            return true;
        }
        return barrierColor == bitmap.getPixel(x, y);
    }

    boolean isFinished(final int x, final int y){
        if(x < 0 || y < 0 || x >= bitmap.getWidth() || y >= bitmap.getHeight()){
            return false;
        }
        return finishColor == bitmap.getPixel(x, y);
    }

    @Override
    public PositionVelocityTransform apply(final float x, final float y) {
        velNew = applyAccel(new float[]{x, y}, applyFriction(freeMoving.getVelocity()));
        point.set(freeMoving.getPoint().x + (int)velNew[0], freeMoving.getPoint().y + (int)velNew[1]);
        final Point paddedPoint = addCollisionTestPadding(point.x, point.y, velNew);
        if(isFinished(paddedPoint.x, freeMoving.getPoint().y) || isFinished(freeMoving.getPoint().x, paddedPoint.y)){
            finished = true;
            return this;
        }
        if(isInHole(paddedPoint.x, freeMoving.getPoint().y) || isInHole(freeMoving.getPoint().x, paddedPoint.y)){
            inHole = true;
            return this;
        }
        //xCoord will be against the object
        if(isAgainstWall(paddedPoint.x, freeMoving.getPoint().y)){
            point.x = freeMoving.getPoint().x;
            velNew[0] = -1*applyImpact(velNew[0]);
        }
        //yCoord will be against the object
        if(isAgainstWall(freeMoving.getPoint().x, paddedPoint.y)){
            point.y = freeMoving.getPoint().y;
            velNew[1] = -1*applyImpact(velNew[1]);
        }
        return this;
    }

    public boolean isInHole() {
        return inHole;
    }

    public boolean isFinished() {
        return finished;
    }
}
