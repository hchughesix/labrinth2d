//Copyright (c) 2014 H. C. Christopher Hughes
package com.hchughesix.mobile.labrinth2d;

import android.graphics.*;
import com.google.common.collect.Lists;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

public class Board {

    private final Bitmap bitmap;
    private final Paint bitmapPaint = new Paint();
    private final List<PhysFreeMoving> freeMovings = Lists.newArrayList();
    private final List<PhysBarrier> barriers = Lists.newArrayList();
    private final List<Hole> holes = Lists.newArrayList();
    private final int width;
    private final int height;

    public Board(final Bitmap bitmap, final int width, final int height) {
        this.bitmap = checkNotNull(bitmap);
        this.width = width;
        this.height = height;
    }

    public boolean isReady() {
        for(PhysFreeMoving object : freeMovings){
            if(!object.isReady()){
                return false;
            }
        }
        return true;
    }

    public void draw(final Canvas canvas) {
        canvas.drawColor(Color.LTGRAY);
        canvas.drawBitmap(bitmap, (getWidth()/2) - (bitmap.getWidth()/2), (getHeight()/2) - (bitmap.getHeight()/2), bitmapPaint);
        for(PhysBarrier barrier : barriers){
            barrier.draw(canvas);
        }
        for (PhysFreeMoving marble : freeMovings){
            marble.draw(canvas);
        }
        for(Hole hole : holes){
            hole.draw(canvas);
        }
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void addHole(final Hole hole){
        this.holes.add(checkNotNull(hole));
    }

    public void addFreeMoving(final PhysFreeMoving movingObj){
        this.freeMovings.add(checkNotNull(movingObj));
    }

    public void addBarrier(final PhysBarrier barrier){
        this.barriers.add(checkNotNull(barrier));
    }

    public Collection<Hole> getHoles(){
        return Collections.unmodifiableCollection(holes);
    }

    public int getFreeMovingsInHoles(){
        int count = 0;
        for (Hole hole : holes){
            count += hole.getFreeMovings().size();
        }
        return count;
    }

    public Collection<PhysBarrier> getBarriers() {
        return Collections.unmodifiableCollection(barriers);
    }

    public Collection<PhysFreeMoving> getFreeMovings() {
        return Collections.unmodifiableCollection(freeMovings);
    }

    public int getPoints() {
        int pointsTotal = 0;
        for(Hole hole : getHoles()){
            pointsTotal  = pointsTotal + hole.getPointsTotal();
        }
        return pointsTotal;
    }

    public void clearPoints() {
        for(Hole hole : getHoles()){
            hole.clearFreeMovings();
        }
    }

}
