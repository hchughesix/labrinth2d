//Copyright (c) 2014 H. C. Christopher Hughes
package com.hchughesix.mobile.labrinth2d;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;

import static com.google.common.base.Preconditions.checkNotNull;

public class Bumper extends PhysBarrier {

    private final Paint paint = new Paint();

    public Bumper(final int color, final RectF rect) {
        super(checkNotNull(rect));
        this.paint.setColor(color);
    }

    @Override
    public boolean isReady() {
        return true;
    }

    @Override
    public void draw(final Canvas canvas) {
        canvas.drawRect(rect, paint);
    }

    @Override
    public Point getPoint() {
        return new Point((int)rect.left, (int)rect.top);
    }

    @Override
    public int getWidth() {
        return Math.round(rect.width());
    }

    @Override
    public int getHeight() {
        return Math.round(rect.height());
    }

}
