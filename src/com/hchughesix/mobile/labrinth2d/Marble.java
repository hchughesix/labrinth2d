//Copyright (c) 2014 H. C. Christopher Hughes
package com.hchughesix.mobile.labrinth2d;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

import static com.google.common.base.Preconditions.checkNotNull;

public class Marble implements PhysFreeMoving {

    private final int radius;
    protected final Board board;
    private final Paint marblePaint = new Paint();
    protected float[] velocity;
    protected Point point;
    private final float friction;
    private final float impactEnergyLoss;
    private final float maxVelocity;
    private final int score;
    private boolean isReady;

    protected Marble(final MarbleBuilder builder) {
        this.radius = builder.radius;
        this.board = builder.board;
        this.score = builder.value;
        marblePaint.setColor(builder.color);
        marblePaint.setStyle(Paint.Style.FILL);
        this.velocity = builder.velocity;
        this.point = builder.point;
        this.friction = builder.friction;
        this.impactEnergyLoss = builder.impactEnergyLoss;
        this.maxVelocity = builder.maxVelocity;
        this.isReady = builder.isReady;
    }

    @Override
    public int getScore() {
        return score;
    }

    public Point getPoint() {
        return point;
    }

    @Override
    public int getWidth() {
        return radius*2;
    }

    @Override
    public int getHeight() {
        return radius*2;
    }

    @Override
    public float[] getVelocity() {
        return new float[]{velocity[0], velocity[1]};
    }

    @Override
    public float getFriction() {
        return friction;
    }

    @Override
    public float getImpactEnergyLoss() {
        return impactEnergyLoss;
    }

    @Override
    public float getMaxVelocity() {
        return maxVelocity;
    }

    public void draw(final Canvas canvas) {
        canvas.drawCircle(point.x, point.y, radius, marblePaint);
    }

    public boolean isReady(){
        return isReady && null != point;
    }

    @Override
    public void setVel(final float[] vel){
        this.velocity = checkNotNull(vel);
    }

    @Override
    public void setPoint(final Point point){
        this.point = checkNotNull(point);
    }

    public static class MarbleBuilder {

        private int value = 1;
        private Board board;
        private int radius = 10;
        private int color = Color.RED;
        private float[] velocity = new float[]{0,0};
        private Point point = null;
        private float friction = .01f;
        private float impactEnergyLoss = .5f;
        private float maxVelocity = 3.5f;
        private boolean isReady = true;

        public MarbleBuilder(final Board board) {
            this.board = board;
            this.point = new Point(board.getWidth()/2, board.getHeight()/2);
        }

        public MarbleBuilder setRadius(int radius) {
            this.radius = radius;
            return this;
        }

        public MarbleBuilder setColor(int color) {
            this.color = color;
            return this;
        }

        public MarbleBuilder setVelocity(float[] velocity) {
            this.velocity = new float[]{velocity[0], velocity[1]};
            return this;
        }

        public MarbleBuilder setPoint(Point point) {
            this.point = point;
            return this;
        }

        public MarbleBuilder setFriction(float friction) {
            this.friction = friction;
            return this;
        }

        public MarbleBuilder setImpactEnergyLoss(float impactEnergyLoss) {
            this.impactEnergyLoss = impactEnergyLoss;
            return this;
        }

        public MarbleBuilder setMaxVelocity(float maxVelocity) {
            this.maxVelocity = maxVelocity;
            return this;
        }

        public MarbleBuilder setValue(int value){
            this.value = value;
            return this;
        }

        public Marble createMarble() {
            return new Marble(this);
        }
    }

}
