//Copyright (c) 2014 H. C. Christopher Hughes
package com.hchughesix.mobile.labrinth2d;

import android.graphics.Point;

public interface PhysFreeMoving extends Phys {

    float[] getVelocity();

    float getFriction();

    float getImpactEnergyLoss();

    float getMaxVelocity();

    int getScore();

    void setPoint(Point point);

    void setVel(float[] vel);

}
