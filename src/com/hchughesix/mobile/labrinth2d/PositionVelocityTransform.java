//Copyright (c) 2014 H. C. Christopher Hughes
package com.hchughesix.mobile.labrinth2d;

import android.graphics.Point;

import static com.google.common.base.Preconditions.checkNotNull;

public abstract class PositionVelocityTransform {
    private static final int C = 10;

    protected float[] velNew = new float[2];
    protected final Point point = new Point();
    protected final PhysFreeMoving freeMoving;

    public PositionVelocityTransform(final PhysFreeMoving physFreeMoving) {
        this.freeMoving = checkNotNull(physFreeMoving);
    }

    protected float[] applyAccel(final float[] acceleration, final float[] velocity) {
        return new float[]{applyAccel(acceleration[0], velocity[0]), applyAccel(acceleration[1], velocity[1])};
    }

    private float applyAccel(final float acceleration, final float velocity) {
        float velProp = (acceleration / C) + velocity;
        if(velProp < 0){
            return velProp < -1* freeMoving.getMaxVelocity() ? -1* freeMoving.getMaxVelocity() : velProp;
        }else {
            return velProp > freeMoving.getMaxVelocity() ? freeMoving.getMaxVelocity() : velProp;
        }
    }

    protected float applyImpact(final float velocity){
        float velocityAfterImpact = velocity;
        if(velocity < 0){
            return velocity + freeMoving.getImpactEnergyLoss() < 0 ? velocity + freeMoving.getImpactEnergyLoss() : 0;
        }else if(velocity > 0){
            return velocity - freeMoving.getImpactEnergyLoss() > 0 ? velocity - freeMoving.getImpactEnergyLoss() : 0;
        }
        return velocityAfterImpact;
    }

    protected float[] applyFriction(final float[] velocity) {
        return new float[]{applyFriction(velocity[0]), applyFriction(velocity[1])};
    }

    private float applyFriction(final float velocity) {
        float velAfterFriction = velocity;
        if(velocity < 0){
            velAfterFriction = velocity + freeMoving.getFriction() < 0 ? velocity + freeMoving.getFriction() : 0;
        }else if(velocity > 0){
            velAfterFriction = velocity - freeMoving.getFriction() > 0 ? velocity - freeMoving.getFriction() : 0;
        }
        return velAfterFriction;
    }

    public float[] getVelNew() {
        return new float[]{velNew[0], velNew[1]};
    }

    public Point getPoint() {
        return point;
    }

    protected Point addCollisionTestPadding(final int x, final int y, final float velocity[]){
        Point paddedPoint = new Point();
        if(velocity[0] < 0){
            paddedPoint.x = x - (freeMoving.getWidth()/2);
        }
        if(velocity[0] > 0){
            paddedPoint.x = x + (freeMoving.getWidth()/2);
        }
        if(velocity[1] < 0){
            paddedPoint.y = y - (freeMoving.getWidth()/2);
        }
        if(velocity[1] > 0){
            paddedPoint.y = y + (freeMoving.getWidth()/2);
        }
        return new Point(x, y);
    }

    public abstract PositionVelocityTransform apply(float x, float y);
}
